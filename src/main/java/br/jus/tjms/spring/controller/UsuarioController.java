package br.jus.tjms.spring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.jus.tjms.spring.components.Paginator;
import br.jus.tjms.spring.model.Usuario;

@Controller
@SessionScope
public class UsuarioController {

	
	
	private List<Usuario> lista;
	
	
	
	public UsuarioController() {
		lista= new ArrayList<>();
		lista.add(new Usuario(1,"Diego","123456","grupo A"));
		lista.add(new Usuario(2,"Daniele","45654","grupo B"));
		lista.add(new Usuario(3,"Carol","554545","grupo C"));
	}
	
	@RequestMapping(value="/usuarios/novo")
	public String novo(Usuario usuario){
		return "usuario";
	}
	
	
	@RequestMapping(value="/usuarios/novo", method= RequestMethod.POST)
	public String cadastrar(@Valid Usuario usuario, BindingResult result, Model model, RedirectAttributes attributes){
		if (result.hasErrors()){
			return novo(usuario);
		}

		attributes.addFlashAttribute("mensagem", "Usuario Salvo com Sucesso");
		System.out.println(">>> usuario: " + usuario);
		return "redirect:/usuarios/novo";
	}
	
	@RequestMapping("/usuarios/{id}")
    public String edit(@PathVariable Integer id, Model model){
        model.addAttribute("usuario", lista.get(lista.indexOf( new Usuario(id) ) ));
        return "usuario";
    }
	
	
	@RequestMapping("/usuarios/delete/{id}")
    public String delete(@PathVariable Integer id){
        lista.remove(lista.indexOf( new Usuario(id) ) );
        return "redirect:/usuarios/lista";
    }
	
	
	@RequestMapping(value="/usuarios/lista")
	public String  lista(Model model,  HttpServletRequest request){
		
		model.addAttribute("lista",lista);
		model.addAttribute("pagina",new Paginator<Usuario>(this.lista, request));
		
		return "usuario-lista";
		
	}
	
	

}
