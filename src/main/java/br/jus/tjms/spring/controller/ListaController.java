package br.jus.tjms.spring.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.jus.tjms.spring.model.Usuario;






@Controller
@RequestMapping("/listas")
public class ListaController {
	
	
	@RequestMapping(value="/lista", produces = "text/html; charset=utf-8")
	public ModelAndView home(){
		
		ModelAndView mv = new ModelAndView("lista");
		
		Usuario usuario = new Usuario();
		usuario.setNome("Diego Daniel");
		mv.addObject("usuario", usuario);
		return mv;

		
	}

}
