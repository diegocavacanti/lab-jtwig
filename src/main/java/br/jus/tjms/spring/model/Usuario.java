package br.jus.tjms.spring.model;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class Usuario {
	
	
	private Integer id;
	
	@NotBlank(message="Nome é obrigatório")
	private String nome;
	
	@Size(min=3 , max=8, message="O tamanho da senha deve ser maior que 3 e menor que 8")
	private String senha;
	
	private String grupo;
	
	
	public Usuario() {
		
	}
	public Usuario(Integer id) {
		this.id = id;
	}
	
	
	
	public Usuario(Integer id,String nome, String senha, String grupo) {
		super();
		this.id = id;
		this.nome = nome;
		this.senha = senha;
		this.grupo = grupo;
	}

	
	


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}



	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}



	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}
	
	
	

}
