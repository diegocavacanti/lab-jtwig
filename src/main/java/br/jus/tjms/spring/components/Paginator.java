package br.jus.tjms.spring.components;



import java.util.List;

import javax.servlet.http.HttpServletRequest;


import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.web.util.UriComponentsBuilder;

public class Paginator<T> {

	private List<T> content;
	
	
	private UriComponentsBuilder uriBuilder;

	private int pageNumber;

	private int totalPages;

	public Paginator(List<T> content, HttpServletRequest httpServletRequest) {
		this.pageNumber = 1;
		this.totalPages = 3;
		this.content = content;
		String httpUrl = httpServletRequest.getRequestURL().append(
				httpServletRequest.getQueryString() != null ? "?" + httpServletRequest.getQueryString() : "").toString().replaceAll("\\+", "%20");
		System.out.println(httpUrl);
		this.uriBuilder = UriComponentsBuilder.fromHttpUrl(httpUrl);
	}
	
	public List<T> getConteudo() {
		return content;
	}
	
	public boolean isVazia() {
		return content.isEmpty();
	}
	
	public int getAtual() {
		return pageNumber;
	}
	
	public boolean isPrimeira() {
		return pageNumber == 1;
	}
	
	public boolean isUltima() {
		return pageNumber == totalPages;
	}
	
	public int getTotal() {
		return totalPages;
	}
	
	public String urlParaPagina(int pagina) {
		return uriBuilder.replaceQueryParam("page", pagina).build(true).encode().toUriString();
	}
	
	public String urlOrdenada(String propriedade) {
		UriComponentsBuilder uriBuilderOrder = UriComponentsBuilder
				.fromUriString(uriBuilder.build(true).encode().toUriString());
		
		String valorSort = String.format("%s,%s", propriedade, inverterDirecao(propriedade));
		
		return uriBuilderOrder.replaceQueryParam("sort", valorSort).build(true).encode().toUriString();
	}
	
	public String inverterDirecao(String propriedade) {
		/*String direcao = "asc";
		
		Order order = page.getSort() != null ? page.getSort().getOrderFor(propriedade) : null;
		if (order != null) {
			direcao = Sort.Direction.ASC.equals(order.getDirection()) ? "desc" : "asc";
		}
		
		return direcao;
		*/
		return "";
	}
	
	public boolean descendente(String propriedade) {
		return inverterDirecao(propriedade).equals("asc");
	}
	
	public boolean ordenada(String propriedade) {
		/*Order order = page.getSort() != null ? page.getSort().getOrderFor(propriedade) : null; 
		
		if (order == null) {
			return false;
		}
		
		return page.getSort().getOrderFor(propriedade) != null ? true : false;
		*/
		return true;
	}
	
}