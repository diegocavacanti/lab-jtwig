package br.jus.tjms.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import br.jus.tjms.spring.service.HomeService;


@Configuration
@ComponentScan(basePackageClasses = HomeService.class)
public class ServiceConfig {

}
